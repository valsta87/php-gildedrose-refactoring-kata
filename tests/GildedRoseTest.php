<?php

declare(strict_types=1);

namespace Tests;

use GildedRose\GildedRose;
use GildedRose\Item;
use GildedRose\ItemProcessor;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    public function testFoo(): void
    {
        $items = [new Item('foo', 0, 0)];
        $itemProcessor = new ItemProcessor();
        $gildedRose = new GildedRose($items, $itemProcessor);
        $gildedRose->updateQuality();
        $this->assertSame('foo', $items[0]->name);
        $itemProcessor->increaseQuality($items[0]);
        $this->assertSame(1, $items[0]->quality);
        $itemProcessor->reduceQuality($items[0]);
        $this->assertSame(0, $items[0]->quality);
    }
}
