<?php

declare(strict_types=1);

namespace GildedRose;

final class GildedRose
{
    public const productName = [
        'agedBrie' => 'Aged Brie',
        'backstagePasses' => 'Backstage passes to a TAFKAL80ETC concert',
        'sulfuras' => 'Sulfuras, Hand of Ragnaros',
    ];

    /**
     * @var Item[]
     */
    private $items;

    /**
     * @var ItemProcessor
     */
    private $itemProcessor;

    public function __construct(array $items, ItemProcessor $itemProcessor)
    {
        $this->items = $items;
        $this->itemProcessor = $itemProcessor;
    }

    public function updateQuality(): void
    {
        foreach ($this->items as $item) {
            if ($item->name === self::productName['agedBrie']) {
                $this->itemProcessor->updateQualityAgedBrie($item);
            } elseif ($item->name === self::productName['backstagePasses']) {
                $this->itemProcessor->updateQualityBackstagePasses($item);
            } elseif ($item->name === self::productName['sulfuras']) {
                $this->itemProcessor->updateQualitySulfuras($item);
            } else {
                $this->itemProcessor->updateQualityBasic($item);
            }
        }
    }
}
