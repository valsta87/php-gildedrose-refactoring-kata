<?php

declare(strict_types=1);

namespace GildedRose;

class ItemProcessor
{
    public function increaseQuality(Item $item): int
    {
        return $item->quality++;
    }

    public function reduceQuality(Item $item): int
    {
        return $item->quality--;
    }

    public function isLessThanFiftyQuality(Item $item): bool
    {
        return $item->quality < 50;
    }

    public function reduceSellIn(Item $item): int
    {
        return $item->sell_in--;
    }

    public function isSellInLessThanNumber(Item $item, int $number): bool
    {
        return $item->sell_in < $number;
    }

    public function updateQualityAgedBrie(Item $item): void
    {
        if ($this->isLessThanFiftyQuality($item)) {
            $this->increaseQuality($item);
        }

        $this->reduceSellIn($item);

        if ($this->isSellInLessThanNumber($item, 0) && $this->isLessThanFiftyQuality($item)) {
            $this->increaseQuality($item);
        }
    }

    public function updateQualityBackstagePasses(Item $item): void
    {
        if ($this->isLessThanFiftyQuality($item)) {
            $this->increaseQuality($item);
        }

        if ($this->isLessThanFiftyQuality($item)) {
            if ($this->isSellInLessThanNumber($item, 11)) {
                $this->increaseQuality($item);
            }

            if ($this->isSellInLessThanNumber($item, 6)) {
                $this->increaseQuality($item);
            }
        }

        $this->reduceSellIn($item);

        if ($this->isSellInLessThanNumber($item, 0)) {
            $item->quality = 0;
        }
    }

    public function updateQualitySulfuras(Item $item): void
    {
        if ($this->isLessThanFiftyQuality($item)) {
            $this->increaseQuality($item);
        }

        if ($this->isSellInLessThanNumber($item, 0) && $this->isLessThanFiftyQuality($item)) {
            $this->increaseQuality($item);
        }
    }

    public function updateQualityBasic(Item $item): void
    {
        if ($item->quality > 0) {
            $this->reduceQuality($item);
        }

        $this->reduceSellIn($item);

        if ($this->isSellInLessThanNumber($item, 0) && $item->quality > 0) {
            $this->reduceQuality($item);
        }
    }
}
